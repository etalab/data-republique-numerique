# data-republique-numerique

Les fichiers JSON récupérés en utilisant l'API du site [de la consultation pour la loi Numérique](https://www.republique-numerique.fr/).

Ce dépôt contient l'ensemble des articles (gouvernementaux et non gouvernementaux), ainsi leur évolution heure par heure sur les 4 derniers jours de la consultation.

Par contre, ce dépôt ne contient pas les versions alternatives aux articles. Celles-ci se trouvent dans un [autre dépôt](https://git.framasoft.org/etalab/opinions-republique-numerique), mais sans historique.

Ces données ont été produites par un [moissonneur sous licence libre](https://git.framasoft.org/etalab/moissonneur-republique-numerique).
Elles sont en "open data", sous [licence ouverte](https://www.etalab.gouv.fr/licence-ouverte-open-licence).

## Réutilisations

Ce jeu de données a été utilisé par plusieurs projets, lors de l'[OpenDataCamp "La vie quotidienne en données"](https://www.etalab.gouv.fr/open-data-camp-la-vie-quotidienne-en-donnees).
Et notamment :

* https://github.com/c24b/project_loi
* https://git.framasoft.org/Forteza/loi-numerique
